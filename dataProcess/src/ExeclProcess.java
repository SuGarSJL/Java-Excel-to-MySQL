import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.DruidJDBCUtils;
import utils.SnowFlakeUtil;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ExeclProcess {
    public static void main(String[] args) throws IOException, WriteException, BiffException, ParseException {
        //1:创建workbook，即excel文件
        Workbook workbook = Workbook.getWorkbook(new File("./excels/2020data.xls"));
        //2:获取所有工作表sheets
        Sheet[] sheets = workbook.getSheets();
        String[] sensorNames = {"应力传感器", "温度传感器", "伸缩缝传感器", "沉降传感器"};
        String[] dbTbNames = {"sensor_stress_long", "sensor_temperature_long", "sensor_expansion_joint_long", "sensor_settlement_long"};
        String[] labels = {"应力监测", "温度监测", "伸缩缝监测", "沉降监测"};
        String[] units = {"MPa", "℃", "", ""};

/*        //1:创建workbook，即excel文件
        Workbook workbook = Workbook.getWorkbook(new File("./excels/202005数据.xls"));
        //2:获取所有工作表sheets
        Sheet[] sheets = workbook.getSheets();
        //3.数据库字段
        String[] sensorNames = {"应力传感器", "温度传感器", "索力传感器"};
        String[] dbTbNames = {"sensor_stress_short", "sensor_temperature_short", "sensor_cable_force_short"};
        String[] labels = {"应力监测", "温度监测", "索力监测"};
        String[] units = {"MPa", "℃", "Pa"};*/


        //4.获取数据库连接池对象
        DataSource ds = DruidJDBCUtils.getDataSource();
        //5.创建JdbcTemplate对象
        JdbcTemplate template = new JdbcTemplate(ds);
        //6.
        String sensor_id = null;
        String name = null;
        String value = null;
        String sql = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time = null;
        //7:获取数据
        for (int i = 0; i < sheets.length; i++) {
            int rows = sheets[i].getRows();
            int columns = sheets[i].getColumns();
            for (int r = 1; r < rows; r++) {
                for (int c = 1; c < columns; c++) {
                    sensor_id = sheets[i].getCell(c, 0).getContents().toLowerCase(Locale.ROOT);//获得第2列开始，每列的第一个代表传感器id

                    name = sensorNames[i] + "_" + sensor_id;
                    System.out.println(((NumberCell)sheets[i].getCell(c, r)).getValue());
                    value = String.valueOf(((NumberCell)sheets[i].getCell(c, r)).getValue());//获得第2列开始，每列的除第一个外的值，代表传感器数据
                    time = sdf.parse(sheets[i].getCell(0, r).getContents());//获取第一列的时间
                    //8.定义SQL
                    sql = "insert into " + dbTbNames[i] + "(" +
                            "id, " +
                            "sensor_id, " +
                            "name, " +
                            "type, " +
                            "label, " +
                            "value, " +
                            "unit, " +
                            "create_time, " +
                            "create_by, " +
                            "update_time, " +
                            "update_by) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    //9.调用方法，执行sql
                    int count = template.update(sql, String.valueOf(SnowFlakeUtil.getFlowIdInstance().nextId()), sensor_id, name, i, labels[i], value, units[i], time, name, time, name);
                    System.out.println(sheets[i].getName() + "：(" + r + ", " + c + ")-(" + value + ")");
                }
            }

        }

        //最后一步：关闭资源
        workbook.close();
    }
}
