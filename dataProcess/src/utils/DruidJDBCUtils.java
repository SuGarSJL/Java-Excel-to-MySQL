package utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DruidJDBCUtils {
    private static DataSource ds;

    static {
        try {
            //1.加载配置文件
            Properties pros = new Properties();
            InputStream is = DruidJDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");
            pros.load(is);
            //2.获取数据库连接池对象
            ds = (DataSource) DruidDataSourceFactory.createDataSource(pros);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * @Author SuGar
     * @Description //TODO 3.获取数据库连接
     * @Date 17:33 2021/2/16
     * @Param []
     * @return java.sql.Connection
     **/
    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    /**
     * @Author SuGar
     * @Description //TODO 释放资源
     * @Date 17:36 2021/2/16
     * @Param [stmt, conn]
     * @return void
     **/
    public static void close(Statement stmt, Connection conn){
        /*if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }*/

        close(null, stmt, conn);
    }

    /**
     * @Author SuGar
     * @Description //TODO 释放资源
     * @Date 17:36 2021/2/16
     * @Param [rs, stmt, conn]
     * @return void
     **/
    public static void close(ResultSet rs, Statement stmt, Connection conn){
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author SuGar
     * @Description //TODO 获取数据库连接池对象
     * @Date 17:39 2021/2/16
     * @Param []
     * @return javax.sql.DataSource
     **/
    public static DataSource getDataSource(){
        return ds;
    }
}
